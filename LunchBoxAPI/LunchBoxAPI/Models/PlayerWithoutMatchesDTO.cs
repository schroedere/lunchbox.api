﻿namespace LunchBoxAPI.Models
{
    public class PlayerWithoutMatchesDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Wins { get; set; } = 0;

        public int Losses { get; set; } = 0;

        public decimal WinPercentage { get; set; } = 0; 


    }
}

