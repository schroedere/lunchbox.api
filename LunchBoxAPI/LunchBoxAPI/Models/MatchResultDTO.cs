﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Models
{
    public class MatchResultDTO
    {
        [Key]
        public int Id { get; set; }

        public int PlayerId { get; set; }

        public int MatchId { get; set; }

        [ForeignKey("MatchId")]
        public virtual MatchDTO Match { get; set; }

        [ForeignKey("PlayerId")]
        public virtual PlayerForMatchDTO Player { get; set; }

        public string Result { get; set; }
    }
}
