﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Models
{
    public class PlayerForCreationDTO
    {
        [Required(ErrorMessage = "Player Name is required")]
        [MaxLength(50, ErrorMessage = "Player Name can be no longer than 50 characters")]
        public string Name { get; set; }

    }
}
