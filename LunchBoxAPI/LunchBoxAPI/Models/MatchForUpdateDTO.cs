﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchBoxAPI.Models
{
    public class MatchForUpdateDTO
    {
        //public int WinningPlayerId { get; set; }
        //public int LosingPlayerId { get; set; }

        //public PlayerDTO WinningPlayer { get; set; }

        //public PlayerDTO LosingPlayer { get; set; }

        public DateTime MatchDate { get; set; }

        [Required(ErrorMessage = "Match must have a Game")]
        public GameDTO Game { get; set; }

        [ForeignKey("GameId")]
        public int GameId { get; set; }
    }
}
