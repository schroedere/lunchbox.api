﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LunchBoxAPI.Models
{
    public class GameDTO
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Game Title can be no longer than 50 characters")]
        public string Title { get; set; }

        public ICollection<MatchDTO> Matches = new List<MatchDTO>();

        public DateTime CreateDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool IsActive { get; set; }
    }
}
