﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Models
{
    public class PlayerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;

        public int Wins { get; set; } = 0;

        public int Losses { get; set; } = 0;

        public decimal WinPercentage { get; set; } = 0; 

        ICollection<MatchResultDTO> MatchResults { get; set; } = new List<MatchResultDTO>(); 
       

        public DateTime CreateDate { get; set; }


        public DateTime ModifiedDate { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
