﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Models
{
    public class GameForCreationDTO
    {
        public string Title { get; set; }
    }
}
