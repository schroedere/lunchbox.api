﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Models
{
    public class MatchResultForCreationDTO
    {
        public int PlayerId { get; set; }

        public string Result { get; set; }

        public int MatchId { get; set; }
    }
}
