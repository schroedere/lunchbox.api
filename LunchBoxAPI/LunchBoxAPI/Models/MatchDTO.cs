﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Models
{
    public class MatchDTO
    {
        [Key]
        public int Id { get; set; }

        public ICollection<MatchResultDTO> MatchResults { get; set; }

        //public int WinningPlayerId { get; set; }
        //public int LosingPlayerId { get; set; }
        //public PlayerDTO WinningPlayer { get; set; }
        //public PlayerDTO LosingPlayer { get; set; }
        public GameDTO Game { get; set; }

        [ForeignKey("GameId")]
        public int GameId { get; set; }

        public DateTime MatchDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool IsActive { get; set; }

    }
}
