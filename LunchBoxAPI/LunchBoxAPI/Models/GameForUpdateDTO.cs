﻿using System.ComponentModel.DataAnnotations;

namespace LunchBoxAPI.Models
{
    public class GameForUpdateDTO
    {
        [Required]
        [MaxLength(50, ErrorMessage = "Game Title can be no longer than 50 characters")]
        public string Title { get; set; }

        public bool IsActive { get; set; }
    }
}
