﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using LunchBoxAPI.Entities;
using LunchBoxAPI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;

namespace LunchBoxAPI
{
    public class Startup
    {
        public static IConfigurationRoot Configuration; 
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appSettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
               .AddEnvironmentVariables();

            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
              .AddMvcOptions(o => o.OutputFormatters.Add(
                  new XmlDataContractSerializerOutputFormatter()));

            var connectionString = Configuration["connectionStrings:LunchBoxDBConnectionString"];


            services.AddDbContext<GameContext>(options => options.UseSqlServer(connectionString));

            services.AddScoped<IGameRepository, GameRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {

                app.UseDeveloperExceptionPage();
            }

            else
            {
                app.UseExceptionHandler();
            }

            app.UseStatusCodePages();

            app.UseMvc();

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.Game, Models.GameWithoutMatchesDTO>();
                cfg.CreateMap<Entities.Game, Models.GameDTO>();
                cfg.CreateMap<Models.GameForUpdateDTO, Entities.Game>();
                cfg.CreateMap<Models.GameForCreationDTO, Entities.Game>();
                cfg.CreateMap<Entities.Game, Models.GameForUpdateDTO>();
                cfg.CreateMap<Entities.Game, Models.GameForMatchDTO>();
                cfg.CreateMap<Models.GameForMatchDTO, Entities.Game>();
                cfg.CreateMap<Entities.Match, Models.MatchDTO>().MaxDepth(5);
                cfg.CreateMap<Models.MatchForCreationDTO, Entities.Match>();
                cfg.CreateMap<Entities.Match, Models.MatchForCreationDTO>();
                cfg.CreateMap<Models.MatchForUpdateDTO, Entities.Match>();
                cfg.CreateMap<Entities.Player, Models.PlayerDTO>();
                cfg.CreateMap<Entities.Player, Models.PlayerWithoutMatchesDTO>();
                cfg.CreateMap<Models.PlayerForCreationDTO, Entities.Player>();
                cfg.CreateMap<Models.PlayerForUpdateDTO, Entities.Player>();
                cfg.CreateMap<Entities.Player, Models.PlayerForMatchDTO>();
                cfg.CreateMap<Models.PlayerForMatchDTO, Entities.Player>();
                cfg.CreateMap<Models.PlayerDTO, Entities.Player>();
                cfg.CreateMap<Models.MatchResultDTO, Entities.MatchResult>();
                cfg.CreateMap<Models.MatchResultForCreationDTO, Entities.MatchResult>();
                cfg.CreateMap<Entities.MatchResult, Models.MatchResultDTO>(); 
            });

        }
    }
}
