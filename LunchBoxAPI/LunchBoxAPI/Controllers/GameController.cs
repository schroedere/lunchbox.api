﻿using AutoMapper;
using LunchBoxAPI.Models;
using LunchBoxAPI.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace LunchBoxAPI.Controllers
{
    [Route("api/games")]
    public class GameController : Controller
    {
        private IGameRepository _gameRepository;

        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        #region Game
        [HttpGet()]
        public IActionResult GetGames(bool includeMatches = false)
        {
            var gameEntities = _gameRepository.GetGames(includeMatches);

            if (includeMatches == true)
            {
                var results = Mapper.Map<IEnumerable<GameDTO>>(gameEntities);
                return Ok(results);
            }
            else
            {
                var results = Mapper.Map<IEnumerable<GameWithoutMatchesDTO>>(gameEntities);
                return Ok(results);
            }
        }

        [HttpGet("{id}", Name = "GetGame")]
        public IActionResult GetGame(int Id, bool includeMatches = false)
        {

            var game = _gameRepository.GetGame(Id, includeMatches);

            if (game == null)
            {
                return NotFound();
            }

            if (includeMatches)
            {

                var gameResult = Mapper.Map<GameDTO>(game);

                return Ok(gameResult);

            }

            var gamesWithoutMatchesResult = Mapper.Map<GameWithoutMatchesDTO>(game);

            return Ok(gamesWithoutMatchesResult);


        }

        [HttpPost]
        public IActionResult CreateGame([FromBody] GameForCreationDTO game)
        {
            if (game == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newGame = Mapper.Map<Entities.Game>(game);

            _gameRepository.AddGame(newGame);

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem occurred handling your request");
            }

            var createdGame = Mapper.Map<GameDTO>(newGame);

            return CreatedAtRoute("GetGame", new
            { id = createdGame.Id }, createdGame);

        }

        [HttpPut("{id}")]
        public IActionResult UpdateGame(int Id, [FromBody] GameForUpdateDTO game)
        {
            if (game == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_gameRepository.GameExists(Id))
            {
                return NotFound();
            }


            var gameEntity = _gameRepository.GetGame(Id, false);

            if (gameEntity == null)
            {
                return NotFound();
            }

            Mapper.Map(game, gameEntity);

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request");
            }

            return NoContent();
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteGame(int Id)
        {
            if (!_gameRepository.GameExists(Id))
            {
                return NotFound();
            }

            var gameEntity = _gameRepository.GetGame(Id, false);

            if (gameEntity == null)
            {
                return NotFound();
            }

            gameEntity.IsActive = false;

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request.");
            }

            //_mailService.Send("Student has been deleted.", $"Student {studentEntity.Name} with Id {studentEntity.Id} was deleted");

            return NoContent();
        }

        [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateGame(int Id, [FromBody] JsonPatchDocument<GameForUpdateDTO> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!_gameRepository.GameExists(Id))
            {
                return NotFound();
            }

            var gameEntity = _gameRepository.GetGame(Id, false);

            if (gameEntity == null)
            {
                return NotFound();
            }

            var gameToPatch = Mapper.Map<GameForUpdateDTO>(gameEntity);

            patchDoc.ApplyTo(gameToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TryValidateModel(gameToPatch);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(gameToPatch, gameEntity);

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request");
            }

            return NoContent();
        }

        #endregion
    }
}


