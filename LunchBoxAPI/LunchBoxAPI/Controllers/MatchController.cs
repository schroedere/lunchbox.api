﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using LunchBoxAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using LunchBoxAPI.Entities;
using LunchBoxAPI.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LunchBoxAPI.Controllers
{
    [Route("api/games/")]
    public class MatchController : Controller
    {
        private IGameRepository _gameRepository;

        public MatchController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }
        [HttpGet("{gameid}/matches")]
        public IActionResult GetMatches(int gameId)
        {
            if (!_gameRepository.GameExists(gameId))
            {
                return NotFound();
            }

            var matchesForGame = _gameRepository.GetMatchesForGame(gameId);

            var matchesForGameResult = Mapper.Map<IEnumerable<MatchDTO>>(matchesForGame);

            return Ok(matchesForGameResult);
        }

        [HttpGet("{gameId}/matches/{id}", Name = "GetMatch")]
        public IActionResult GetMatch(int gameId, int Id, DateTime matchDate)
        {

            if (!_gameRepository.GameExists(gameId))
            {
                return NotFound();
            }

            if (matchDate != null)
            {
                if (DateTime.TryParse(matchDate.ToString(), out DateTime result))
                {
                    var matchesByDate = _gameRepository.GetMatchesByDate(matchDate);

                    if (matchesByDate == null)
                    {
                        return NotFound();
                    }

                    var matchesByGameAndDateResult = Mapper.Map<IEnumerable<MatchDTO>>(matchesByDate);

                    return Ok(matchesByGameAndDateResult);
                }

                else
                {
                    return BadRequest();
                }

            }

            var match = _gameRepository.GetMatchForGame(gameId, Id);

            var matchforGameResult = Mapper.Map<MatchDTO>(match);

            return Ok(matchforGameResult);
        }

        [HttpPost("{gameId}/matches")]
        public IActionResult CreateMatch(int gameId, [FromBody] MatchForCreationDTO match)
        {
            if (match == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_gameRepository.GameExists(gameId))
            {
                var newMatch = Mapper.Map<Entities.Match>(match);

                _gameRepository.AddMatchForGame(gameId, newMatch);

                if (!_gameRepository.Save())
                {
                    return StatusCode(500, "A problem occurred handling your request");
                }

                var createdMatch = Mapper.Map<MatchDTO>(newMatch);

                return CreatedAtRoute("GetMatch", new
                { id = createdMatch.Id }, createdMatch);
            }
            else
            {
                return BadRequest();
            }
            
        }

        [HttpPut("{gameId}/matches/{id}")]
        public IActionResult UpdateMatch(int gameId, int Id, [FromBody] MatchForUpdateDTO match)
        {
            if (match == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_gameRepository.MatchExists(Id))
            {
                return NotFound();
            }

            var matchEntity = _gameRepository.GetMatchForGame(gameId, Id);

            if (matchEntity == null)
            {
                return NotFound();
            }

            Mapper.Map(match, matchEntity);

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request");
            }

            return NoContent();
        }


        [HttpDelete("{gameId}/matches/{id}")]
        public IActionResult DeleteMatch(int gameId, int Id)
        {
            if (!_gameRepository.MatchExists(Id))
            {
                return NotFound();
            }

            var matchEntity = _gameRepository.GetMatchForGame(gameId, Id);

            if (matchEntity == null)
            {
                return NotFound();
            }

            matchEntity.IsActive = false;

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request.");
            }

            //_mailService.Send("Student has been deleted.", $"Student {studentEntity.Name} with Id {studentEntity.Id} was deleted");

            return NoContent();
        }

        [HttpPatch("{gameId}/matches/{id}")]
        public IActionResult PartiallyUpdateMatch(int gameId, int Id, [FromBody] JsonPatchDocument<MatchForUpdateDTO> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!_gameRepository.MatchExists(Id))
            {
                return NotFound();
            }

            var matchEntity = _gameRepository.GetMatchForGame(gameId, Id);

            if (matchEntity == null)
            {
                return NotFound();
            }

            var matchToPatch = Mapper.Map<MatchForUpdateDTO>(matchEntity);

            patchDoc.ApplyTo(matchToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TryValidateModel(matchToPatch);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(matchToPatch, matchEntity);

            if (!_gameRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request");
            }

            return NoContent();
        }
    }
}


