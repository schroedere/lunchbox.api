﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LunchBoxAPI.Services;
using AutoMapper;
using LunchBoxAPI.Models;
using Microsoft.AspNetCore.JsonPatch;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LunchBoxAPI.Controllers
{
       
        [Route("api/players")]
        public class PlayerController : Controller
        {
            private IGameRepository _gameRepository;

            public PlayerController(IGameRepository gameRepository)
            {
                _gameRepository = gameRepository;
            }

            [HttpGet()]
            public IActionResult GetPlayers(bool includeMatches = false)
            {
                var playerEntities = _gameRepository.GetPlayers(includeMatches);

                if (includeMatches == true)
                {
                    var results = Mapper.Map<IEnumerable<PlayerDTO>>(playerEntities);
                    return Ok(results);
                }
                else
                {
                    var results = Mapper.Map<IEnumerable<PlayerWithoutMatchesDTO>>(playerEntities);
                    return Ok(results);
                }
            }

            [HttpGet("{id}", Name = "GetPlayer")]
            public IActionResult GetPlayer(int Id, bool includeMatches = false)
            {

                var player = _gameRepository.GetPlayer(Id, includeMatches);

                if (player == null)
                {
                    return NotFound();
                }

                if (includeMatches)
                {

                    var playerResult = Mapper.Map<GameDTO>(player);

                    return Ok(playerResult);

                }

                var playerWithoutMatchesResult = Mapper.Map<PlayerWithoutMatchesDTO>(player);

                return Ok(playerWithoutMatchesResult);
            }

            [HttpPost]
            public IActionResult CreatePlayer([FromBody] PlayerForCreationDTO player)
            {
                if (player == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var newPlayer = Mapper.Map<Entities.Player>(player);

                _gameRepository.AddPlayer(newPlayer);

                if (!_gameRepository.Save())
                {
                    return StatusCode(500, "A problem occurred handling your request");
                }

                var createdPlayer = Mapper.Map<PlayerDTO>(newPlayer);

                return CreatedAtRoute("GetPlayer", new
                { id = createdPlayer.Id }, createdPlayer);

            }

            [HttpPut("{id}")]
            public IActionResult UpdatePlayer(int Id, [FromBody] PlayerForUpdateDTO player)
            {
                if (player == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (!_gameRepository.PlayerExists(Id))
                {
                    return NotFound();
                }

                var playerEntity = _gameRepository.GetPlayer(Id, false);

                if (playerEntity == null)
                {
                    return NotFound();
                }

                Mapper.Map(player, playerEntity);

                if (!_gameRepository.Save())
                {
                    return StatusCode(500, "A problem happend while handling your request");
                }

                return NoContent();
            }


            [HttpDelete("{id}")]
            public IActionResult DeletePlayer(int Id)
            {
                if (!_gameRepository.PlayerExists(Id))
                {
                    return NotFound();
                }

                var playerEntity = _gameRepository.GetPlayer(Id, false);

                if (playerEntity == null)
                {
                    return NotFound();
                }

                playerEntity.IsActive = false;

                if (!_gameRepository.Save())
                {
                    return StatusCode(500, "A problem happend while handling your request.");
                }

                //_mailService.Send("Student has been deleted.", $"Student {studentEntity.Name} with Id {studentEntity.Id} was deleted");

                return NoContent();
            }

            [HttpPatch("{id}")]
            public IActionResult PartiallyUpdatePlayer(int Id, [FromBody] JsonPatchDocument<PlayerForUpdateDTO> patchDoc)
            {
                if (patchDoc == null)
                {
                    return BadRequest();
                }

                if (!_gameRepository.PlayerExists(Id))
                {
                    return NotFound();
                }

                var playerEntity = _gameRepository.GetPlayer(Id, false);

                if (playerEntity == null)
                {
                    return NotFound();
                }

                var playerToPatch = Mapper.Map<PlayerForUpdateDTO>(playerEntity);

                patchDoc.ApplyTo(playerToPatch, ModelState);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                TryValidateModel(playerToPatch);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                Mapper.Map(playerToPatch, playerEntity);

                if (!_gameRepository.Save())
                {
                    return StatusCode(500, "A problem happend while handling your request");
                }

                return NoContent();
            }
        }
    }
