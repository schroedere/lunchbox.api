﻿using AutoMapper;
using LunchBoxAPI.Models;
using LunchBoxAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace LunchBoxAPI.Controllers
{
    [Route("api/matches/")]
    public class MatchResultController : Controller
    {
        private IGameRepository _repository;

        public MatchResultController(IGameRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}/results")]
        public IActionResult GetMatchResults(int Id)
        {
            if (!_repository.MatchExists(Id))
            {
                return NotFound();
            }

            var matchResultEntities = _repository.GetMatchResultsByMatchId(Id);

            var results = Mapper.Map<MatchResultDTO>(matchResultEntities);

            return Ok(results); 
        }

        [HttpGet("{id}/results/{matchid}", Name = "GetMatchResult")]
        public IActionResult GetMatchResult(int matchId, int Id)
        {
            if (!_repository.MatchExists(Id))
            {
                return NotFound(); 
            }

            if (_repository.MatchResultExists(Id))
            {
                var resultEntity = _repository.GetMatchResult(Id);

                var result = Mapper.Map<MatchResultDTO>(resultEntity);

                return Ok(result); 
            }
            else
            {
                return NotFound(); 
            }
        }

        [HttpPost("{matchId}/results")]
        public IActionResult CreateMatchResult(int matchId, [FromBody] MatchResultForCreationDTO result)
        {
            if (result == null)
            {
                return BadRequest(); 
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repository.MatchExists(matchId))
            {
                var newMatchResult = Mapper.Map<Entities.MatchResult>(result);

                _repository.AddMatchResult(newMatchResult);

                if (!_repository.Save())
                {
                    return StatusCode(500, "A problem occurred handling your request");
                }

                var createdMatchResult = Mapper.Map<MatchDTO>(newMatchResult);

                return CreatedAtRoute("GetMatchResult", new
                { id = createdMatchResult.Id }, createdMatchResult);
            }
            else
            {
                return BadRequest();
            }
        
        }
    }
}
