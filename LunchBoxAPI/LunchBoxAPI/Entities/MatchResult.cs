﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchBoxAPI.Entities
{
    public class MatchResult
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        public int PlayerId { get; set; }

        public int MatchId { get; set; }

        [ForeignKey("PlayerId")]
        public virtual Player Player { get; set; }

        public string Result { get; set; }

        [ForeignKey("MatchId")]
        public virtual Match Match { get; set; }

    }
}
