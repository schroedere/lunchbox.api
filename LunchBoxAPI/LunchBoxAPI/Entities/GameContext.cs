﻿using Microsoft.EntityFrameworkCore;

namespace LunchBoxAPI.Entities
{
    public class GameContext : DbContext
    {
        public GameContext(DbContextOptions<GameContext> options) : base(options)
        {
            //Database.EnsureCreated();
            Database.Migrate(); 
        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Match> Matches { get; set; }

        public DbSet<MatchResult> MatchResults { get; set; }


    }
}
