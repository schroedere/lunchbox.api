﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LunchBoxAPI.Entities
{
    public class Player
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Player Name is required")]
        [MaxLength(50, ErrorMessage = "Player Name can not be longer than 50 characters")]
        public string Name { get; set; } = string.Empty;

        public int Wins
        {
            get
            {
                if (MatchResults.Count() > 0 && MatchResults.Any(m => m.PlayerId == Id))
                {
                    return MatchResults.Where(m => m.Result == "W" && m.PlayerId == Id).Count();

                }

                else
                {
                    return 0;
                }
            }
        }

        public int Losses
        {
            get
            {
                if (MatchResults.Count() > 0 && MatchResults.Any(m => m.PlayerId == Id))
                {
                    return MatchResults.Where(m => m.Result == "L" && m.PlayerId == Id).Count();

                }

                else
                {
                    return 0;
                }
            }

        }

        public decimal WinPercenatge
        {
            get
            {
                if (Wins == 0 && Losses == 0)
                {
                    return 0;
                }
                else
                {
                    return Wins / Wins + Losses;

                }
            }
        }

        public ICollection<MatchResult> MatchResults { get; set; } = new List<MatchResult>();

        public DateTime CreateDate { get; set; } = DateTime.Now;

        public DateTime ModifiedDate { get; set; } = DateTime.Now;

        public bool IsActive { get; set; } = true;


    }
}
