﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using LunchBoxAPI.Entities;

namespace LunchBoxAPI.Migrations
{
    [DbContext(typeof(GameContext))]
    [Migration("20170424134017_initialmigration")]
    partial class initialmigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("LunchBoxAPI.Entities.Game", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Games");
                });

            modelBuilder.Entity("LunchBoxAPI.Entities.Match", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("GameId");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime>("MatchDate");

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<int?>("PlayerId");

                    b.Property<int?>("PlayerId1");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.HasIndex("PlayerId");

                    b.HasIndex("PlayerId1");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("LunchBoxAPI.Entities.MatchResult", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("MatchId");

                    b.Property<int>("PlayerId");

                    b.Property<string>("Result");

                    b.HasKey("Id");

                    b.HasIndex("MatchId");

                    b.HasIndex("PlayerId");

                    b.ToTable("MatchResults");
                });

            modelBuilder.Entity("LunchBoxAPI.Entities.Player", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("LunchBoxAPI.Entities.Match", b =>
                {
                    b.HasOne("LunchBoxAPI.Entities.Game", "Game")
                        .WithMany("Matches")
                        .HasForeignKey("GameId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LunchBoxAPI.Entities.Player")
                        .WithMany("LosingPlayerMatches")
                        .HasForeignKey("PlayerId");

                    b.HasOne("LunchBoxAPI.Entities.Player")
                        .WithMany("WinningPlayerMatches")
                        .HasForeignKey("PlayerId1");
                });

            modelBuilder.Entity("LunchBoxAPI.Entities.MatchResult", b =>
                {
                    b.HasOne("LunchBoxAPI.Entities.Match", "Match")
                        .WithMany()
                        .HasForeignKey("MatchId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LunchBoxAPI.Entities.Player", "Player")
                        .WithMany()
                        .HasForeignKey("PlayerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
