﻿using LunchBoxAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Services
{
    public class GameRepository : IGameRepository
    {
        private GameContext _context;

        public GameRepository(GameContext context)
        {
            _context = context;
        }
        #region Games

        public bool GameExists(int gameId)
        {
            return _context.Games.Any(g => g.Id == gameId);

        }

        public Game GetGame(int gameId, bool includeMatches)
        {
            if (includeMatches)
            {
                return _context.Games.Include(g => g.Matches)
                    .Where(g => g.Id == gameId).FirstOrDefault();
            }
            else
            {
                return _context.Games.Where(g => g.Id == gameId).FirstOrDefault();
            }
        }

        public IEnumerable<Game> GetGames(bool includeMatches)
        {
            if (includeMatches == true)
            {
                return _context.Games.Where(g => g.IsActive == true).Include(g => g.Matches).OrderBy(g => g.Title).ToList();
            }
            else
            {
                return _context.Games.Where(g => g.IsActive == true).OrderBy(g => g.Title).ToList();
            }

        }

        public void AddGame(Game game)
        {
            _context.Add(game);
        }
        #endregion

        #region Matches
        public void AddMatchForGame(int gameId, Match match)
        {
            var game = GetGame(gameId, false);

            game.Matches.Add(match);
        }

        public IEnumerable<Match> GetMatchesForGame(int gameId)
        {
            return _context.Matches
               .Where(m => m.GameId == gameId).ToList();
        }

        public IEnumerable<Match> GetMatchesByDate(DateTime matchDate)
        {
            return _context.Matches
                .Where(m => m.MatchDate.Date == matchDate.Date).ToList();
        }

        public Match GetMatchForGame(int gameId, int matchId)
        {
            return _context.Matches.Where(m => m.Id == matchId && m.GameId == gameId).FirstOrDefault();
        }

        public void DeleteMatchForGame()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Match> GetMatchesByDateRange(DateTime startDate, DateTime endDate)
        {
            return _context.Matches
                .Where(m => m.MatchDate >= startDate && m.MatchDate <= endDate && m.IsActive == true);
        }

        public bool MatchExists(int Id)
        {
            return _context.Matches.Any(m => m.Id == Id);
        }

        #endregion

        #region Match Results

        public IEnumerable<MatchResult> GetMatchResultsByMatchId(int Id)
        {
            return _context.MatchResults.Where(r => r.MatchId == Id); 
        }

        public void AddMatchResult(MatchResult result)
        {
            _context.Add(result); 
        }

        public MatchResult GetMatchResult(int Id)
        {
            return _context.MatchResults.Where(r => r.Id == Id).FirstOrDefault(); 
        }

        public bool MatchResultExists(int Id)
        {
            return _context.MatchResults.Any(r => r.Id == Id); 
        }


        #endregion

        #region Player
        public void AddPlayer(Player player)
        {
            _context.Add(player);
        }
        public bool PlayerExists(int Id)
        {
            return _context.Players.Any(p => p.Id == Id);
        }

        public Player GetPlayer(int playerId, bool includeMatches)
        {
            if (includeMatches)
            {
                return _context.Players.Include(p => p.MatchResults.OrderBy(m => m.Match.MatchDate)).Where(p => p.Id == playerId).FirstOrDefault(); 
            }
            else
            {
                return _context.Players.Where(p => p.Id == playerId).FirstOrDefault();
            }
        }

        public IEnumerable<Player> GetPlayers(bool includeMatches)
        {
            if (includeMatches)
            {
                return _context.Players.Include(p => p.MatchResults.OrderBy(m => m.Match.MatchDate));

            }
            else
            {
                return _context.Players.OrderBy(p => p.Name);
            }
        }

        public void DeletePlayer()
        {
            throw new NotImplementedException();
        }
        #endregion
        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }        
    }
}
