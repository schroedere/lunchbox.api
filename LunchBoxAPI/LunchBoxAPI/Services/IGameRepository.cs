﻿using LunchBoxAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchBoxAPI.Services
{

    public interface IGameRepository
    {
        #region Games
        IEnumerable<Game> GetGames(bool includeMatches);

        Game GetGame(int gameId, bool includeMatches);

        void AddGame(Game game);

        bool GameExists(int gameId);
        #endregion

        #region Matches
        Match GetMatchForGame(int gameId, int matchId);

        IEnumerable<Match> GetMatchesForGame(int gameId);

        IEnumerable<Match> GetMatchesByDate(DateTime matchDate);

        IEnumerable<Match> GetMatchesByDateRange(DateTime startDate, DateTime endDate);
        void AddMatchForGame(int gameId, Match match);

        void DeleteMatchForGame();

        bool MatchExists(int Id);
        #endregion

        #region Match Results

        IEnumerable<MatchResult> GetMatchResultsByMatchId(int Id);

        void AddMatchResult(MatchResult result);

        MatchResult GetMatchResult(int Id);

        bool MatchResultExists(int Id); 

        #endregion

        #region Players
        IEnumerable<Player> GetPlayers(bool includeMatches);

        Player GetPlayer(int playerId, bool includeMatches);

        void AddPlayer(Player player);

        bool PlayerExists(int Id);

        #endregion
        bool Save();

    }
}

